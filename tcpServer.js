const net = require('net');
const port = 4500;
const host = '192.168.2.110';

//let sockets = [];

const server = net.createServer();
server.listen(port, host, () => {
    console.log('TCP Server is running on port ' + port + '.');
});

let interv = null;
server.on('connection', function(sock) {
    console.log('CONNECTED: ' + sock.remoteAddress + ':' + sock.remotePort);

    //sock.write('Connected!\n');
    // interv = setInterval(()=>{
    //     if(sock){
    //         try{
    //             sock.write(`{"sid":"", ani":"3132830998","response":"ACK!","timestamp":${Date.now()}}\n`);
    //             console.log('tick');
    //         } catch (err) {
    //             console.log({err});
    //         }
    //     }
    // },10000);

    sock.on('data', function(data) {
        console.log('DATA \n' + data);
        //if(global.sockets[0]) global.sockets[0].write('Received!');
        // // Write the data back to all the connected, the client will receive it as data from the server
        // sockets.forEach(function(sock, index, array) {
        //     sock.write(sock.remoteAddress + ':' + sock.remotePort + " said " + data + '\n');
        // });
    });

    // Add a 'close' event handler to this instance of socket
    sock.on('close', function(data) {
        let index = global.sockets.findIndex(function(o) {
            return o.remoteAddress === sock.remoteAddress && o.remotePort === sock.remotePort;
        })
        if (index !== -1) global.sockets.splice(index, 1);
        console.log('CLOSED: ' + sock.remoteAddress + ' ' + sock.remotePort);
        //if(interv) clearInterval(interv);
    });

    global.sockets.push(sock);

});
