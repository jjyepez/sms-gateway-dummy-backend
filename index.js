const express = require('express');
const app = express();

const net = require('net');
const api_host = '192.168.2.110';
const api_port = 4555;
const tcp_port = 4500;
const tcp_host = '192.168.2.110';

let daSocket = null;
let smsQueue = [];

app
    .use(require('morgan')('dev'))
    .use(express.json())

    .all('/sms', (req, res) => {
        const {
            ani,
            body
        } = req.body;

        console.log('REQUEST');
        console.log(JSON.stringify(req.body));
       
        setTimeout(() => {
            if(daSocket){
                const message = 'RANDOM MSG.';
                console.log('Respondiendo');
                smsQueue.push('!RSP:' + Date.now() + ':' + ani + ':' + message);
            }
        }, 10000);
       
        res
            .status(200)
            .json({
                data:'OK ' + Date.now()
            })
        ;
    })
;

app.listen(api_port, api_host, () => {
    console.log('API iniciado en ' + api_port);
});

const server = net.createServer();
server.listen(tcp_port, tcp_host, () => {
    console.log('TCP Server is running on port ' + tcp_port + '.');
});

let interv = null;
server.on('connection', function(sock) {
    console.log('CONNECTED: ' + sock.remoteAddress + ':' + sock.remotePort);

    const socketHash = sock.remoteAddress + ':' + sock.remotePort;

    daSocket = sock;
    //daSocket.write(`Connected:${socketHash}\n`);
    
    interv = setInterval(()=>{
        if(daSocket){
            try{
                if(smsQueue.some(sms => sms!='')){
                    smsQueue.forEach((sms,i) => {
                        if(sms!==''){
                            daSocket.write(`${sms}\n`);
                            smsQueue[i]="";
                        }

                    })
                }
            } catch (err) {
                console.log({err});
            }
        }
    },500);

    //interv2 = setInterval(()=>{
        //smsQueue.push('!PING: ' + Date.now());
    //},10000);

    daSocket.on('data', function(data) {

        console.log('DATA \n' + data.toString());
        const jsonData = JSON.parse(data.toString());
        const ani = jsonData['ani'] || '+573132830998';
        
        setTimeout(()=>{
            const msg = 'GRACIAS POR COMUNICARSE CON XYZ';
            console.log('Respondiendo '+msg)
            smsQueue.push('!RSP:' + Date.now() + ':' + ani + ':' + msg);
        }, 5000);

    });

    // Add a 'close' event handler to this instance of socket
    daSocket.on('close', function(data) {
        //clearInterval(interv2);
        if(!daSocket) return;
        console.log('CLOSING: ' + daSocket.remoteAddress + ' ' + daSocket.remotePort);
        daSocket = null;
    });

});
